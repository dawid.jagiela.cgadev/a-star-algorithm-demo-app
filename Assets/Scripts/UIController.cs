// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using UnityEngine;
using UnityEngine.UI;


public class UIController : MonoBehaviour
{

    [SerializeField] private Toggle placeObstaclesToggle;
    [SerializeField] private Toggle placePawnToggle;
    [SerializeField] private Toggle pawnMovementToggle;

    [Space]
    [SerializeField] private MapController mapController;


    private void Start()
    {
        RegisterEvents();
        placeObstaclesToggle.isOn = true;
    }

    private void RegisterEvents()
    {
        placeObstaclesToggle.onValueChanged.AddListener(OnPlaceObstaclesToggleChangedHandler);
        placePawnToggle.onValueChanged.AddListener(OnPlacePawnToggleChangedHandler);
        pawnMovementToggle.onValueChanged.AddListener(OnPawnMovementToggleChangedHandler);
    }

    private void OnPlaceObstaclesToggleChangedHandler(bool _val)
    {
        mapController.UpdateMapState(EMapState.PlacingObstacles);
    }

    private void OnPlacePawnToggleChangedHandler(bool _val)
    {
        mapController.UpdateMapState(EMapState.PlacingPawn);
    }

    private void OnPawnMovementToggleChangedHandler(bool _val)
    {
        mapController.UpdateMapState(EMapState.PawnMovement);
    }

    private void OnDestroy()
    {
        UnregisterEvents();
    }

    private void UnregisterEvents()
    {
        placeObstaclesToggle.onValueChanged.RemoveListener(OnPlaceObstaclesToggleChangedHandler);
        placePawnToggle.onValueChanged.RemoveListener(OnPlacePawnToggleChangedHandler);
        pawnMovementToggle.onValueChanged.RemoveListener(OnPawnMovementToggleChangedHandler);
    }
}
