// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using System;
using System.Collections.Generic;
using UnityEngine;


public static class GameEvents
{

    public static event Action<Vector3> OnPawnLocationChangeRequest;
    public static event Action<List<Vector3>, Vector2Int, Action<Vector2Int>, Action> OnFollowPathRequest;

    public static void PublishOnPawnLocationChangeRequest(Vector3 _location)
    {
        OnPawnLocationChangeRequest?.Invoke(_location);
    }

    public static void PublishOnFollowPathRequest(List<Vector3> _pathAsWoldPositions, Vector2Int _endOfPathCoords, 
        Action<Vector2Int> _pathFollowFinishedCallback, Action _pathSegmentCompletedCallback)
    {
        OnFollowPathRequest?.Invoke(_pathAsWoldPositions, _endOfPathCoords, _pathFollowFinishedCallback, _pathSegmentCompletedCallback);
    }

}
