// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using System;
using UnityEngine;


public class MapCell : MonoBehaviour
{

    [Space]
    [SerializeField] private Color normalColor;
    [SerializeField] private Color hoveredBorderColor;
    [SerializeField] private Color nonHoveredBorderColor;

    [Space]
    [SerializeField] private Color obstacleColor;
    [SerializeField] private Color obstacleBorderColor;

    [Space]
    [SerializeField] private float borderWidth;

    [Space]
    [SerializeField] SpriteRenderer foreground;
    [SerializeField] SpriteRenderer border;
    [SerializeField] SpriteRenderer centralPoint;
    [SerializeField] BoxCollider2D boxCollider;

    private bool isObstacle = false;

    public Vector2Int GridPostion { get; private set; }

    public event Action<MapCell> OnClick;


    public void Initialize(Vector2 _size, Vector2Int _gridPosition)
    {
        GridPostion = _gridPosition;
        SetSize(_size);
        SetAsObstacle(_obstacle: false);
        border.color = nonHoveredBorderColor;
    }

    private void SetSize(Vector2 _size)
    {
        border.size = _size;
        foreground.size = new Vector2(_size.x - borderWidth * 2, _size.y - borderWidth * 2);
        boxCollider.size = _size;
    }

    public void SetAsObstacle(bool _obstacle)
    {
        if(_obstacle)
        {
            foreground.color = obstacleColor;
            border.color = obstacleBorderColor;
            centralPoint.gameObject.SetActive(false);
        }
        else
        {
            foreground.color = normalColor;
            border.color = hoveredBorderColor;
            centralPoint.gameObject.SetActive(true);
        }
        isObstacle = _obstacle;
    }

    private void OnMouseDown()
    {
        OnClick?.Invoke(this);
    }

    private void OnMouseEnter()
    {
        if(isObstacle) { return; }
        border.color = hoveredBorderColor;
    }

    private void OnMouseExit()
    {
        if(isObstacle) { return; }
        border.color = nonHoveredBorderColor;
    }

}
