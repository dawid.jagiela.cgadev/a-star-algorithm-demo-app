// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;


public class TMProURL : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    [SerializeField] private TextMeshProUGUI TMProLink;
    [SerializeField] private Color normalColor;
    [SerializeField] private Color hoveredColor;


    private void Start()
    {
        TMProLink.color = normalColor;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (TMProLink.textInfo.linkInfo.Length == 0) { return; }
        TMP_LinkInfo linkInfo = TMProLink.textInfo.linkInfo[0];
        string url = linkInfo.GetLinkID();
        Application.OpenURL(url);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        TMProLink.color = hoveredColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TMProLink.color = normalColor;
    }


}
