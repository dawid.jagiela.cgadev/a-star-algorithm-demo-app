// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Pawn : MonoBehaviour
{

    [SerializeField] float speed;

    private Vector3 startingPosition;

    private Action<Vector2Int> movementCompletedCallback;
    private Action pathSegementCompletedCallback;

    private bool movementInProgress;

    private Vector2Int endOfPathCoords;

    private void Start()
    {
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        GameEvents.OnPawnLocationChangeRequest += OnPawnLocationChangeRequestHandler;
        GameEvents.OnFollowPathRequest += OnFollowPathRequestHandler;
    }


    private void OnPawnLocationChangeRequestHandler(Vector3 _targetPosition)
    {
        transform.position = _targetPosition;
    }

    private void OnFollowPathRequestHandler(List<Vector3> _path, Vector2Int _endOfPathCoords,
        Action<Vector2Int> _movementCompletedCallback, Action _pathSegmentCompletedCallback)
    {
        if(movementInProgress) { return; }

        endOfPathCoords = _endOfPathCoords;
        movementCompletedCallback = _movementCompletedCallback;
        pathSegementCompletedCallback = _pathSegmentCompletedCallback;
        MoveAlongPath(_path);
    }


    private void MoveAlongPath(List<Vector3> _path)
    {
        StartCoroutine(MoveAlongPathCoroutine(_path));
    }

    private IEnumerator MoveAlongPathCoroutine(List<Vector3> _path)
    {
        movementInProgress = true;
        for(int i=_path.Count-1; i>=0; i--)
        {
            yield return MoveCoroutine(_path[i]);
        }
        NotifyMovementCompleted(endOfPathCoords);
        movementInProgress = false;
    }

    private IEnumerator MoveCoroutine(Vector3 _targetLocation)
    {
        startingPosition = transform.position;
        float distance = Vector3.Distance(startingPosition, _targetLocation);
        float movementTime = distance / speed;
        float elapsedTime = 0f;
        while(true)
        {
            elapsedTime += Time.deltaTime;
            elapsedTime = Mathf.Clamp(elapsedTime, 0f, movementTime);
            transform.position = Vector3.Lerp(startingPosition, _targetLocation, elapsedTime/movementTime);
            if(elapsedTime >= movementTime)
            {
                NotifyPathSegmentCompleted();
                yield break;
            }
            yield return null;
        }
    }

    private void NotifyPathSegmentCompleted()
    {
        pathSegementCompletedCallback?.Invoke();
    }

    private void NotifyMovementCompleted(Vector2Int gridPostionOnFinish)
    {
        movementCompletedCallback?.Invoke(gridPostionOnFinish);
    }


    private void OnDestroy()
    {
        UnregisterEvents();
    }

    private void UnregisterEvents()
    {
        GameEvents.OnPawnLocationChangeRequest -= OnPawnLocationChangeRequestHandler;
        GameEvents.OnFollowPathRequest -= OnFollowPathRequestHandler;
    }

}
