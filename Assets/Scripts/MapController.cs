// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MapController : MonoBehaviour
{

    [SerializeField] private Vector2Int mapSize;
    [SerializeField] private Vector2 cellSize;
    [SerializeField] private float gapBetweenCells;

    [Space]
    [SerializeField] private MapCell cellPrefab;

    [Space]
    [SerializeField] private Transform mapGridOrigin;
    [SerializeField] private PathVisualizer pathVisualizer;

    [Space]
    [SerializeField] private bool debugLogs = true;


    private MapCellInfo[,] mapCells;
    private Vector2Int pawnGridPosition;
    private EMapState mapState;

    private PathFinder pathFinder;
    private bool followPathInProgress;


    private IEnumerator Start()
    {
        yield return null;
        Initialize();
    }

    private void Initialize()
    {
        mapCells = new MapCellInfo[mapSize.x, mapSize.y];
        pathFinder = new PathFinder(mapCells);
        SpawnCells();
        RegisterEvents();
        SetPawnInitialPosition();
        UpdateMapState(EMapState.PlacingObstacles);
    }

    private void SpawnCells()
    {
        for(int y = 0; y < mapSize.y; y++)
            for(int x = 0; x < mapSize.x; x++)
            {
                var mapCell = Instantiate(cellPrefab, transform);
                mapCell.Initialize(cellSize, new Vector2Int(x, y));
                mapCell.transform.localPosition = GetDesiredCellPostion(x, y);
                mapCells[x, y].mapCell = mapCell;
            }
    }

    private void RegisterEvents()
    {
        for(int y = 0; y < mapSize.y; y++)
            for (int x = 0; x < mapSize.x; x++)
                mapCells[x, y].mapCell.OnClick += MapCell_OnClick;
    }

    private void MapCell_OnClick(MapCell _mapCell)
    {
        ResolveCellClick(_mapCell);
#if UNITY_EDITOR
        if(debugLogs)
        {
            PrintLogs_DEBUG(_mapCell.GridPostion.x, _mapCell.GridPostion.y);
        }
#endif
    }

    private void ResolveCellClick(MapCell _mapCell)
    {
        switch(mapState)
        {
            case EMapState.PlacingObstacles: ResolveClick_PlacingObstacles(_mapCell);
                break;
            case EMapState.PlacingPawn: ResolveClick_PlacingPawn(_mapCell);
                break;
            case EMapState.PawnMovement: ResolveClick_PawnMovement(_mapCell);
                break;
        }
    }

    private void SetPawnInitialPosition()
    {
        GameEvents.PublishOnPawnLocationChangeRequest(mapCells[0,0].mapCell.transform.position);
    }

    public void UpdateMapState(EMapState newMapState)
    {
        mapState = newMapState;
    }

    private void ResolveClick_PlacingObstacles(MapCell _mapCell)
    {
        Vector2Int gridLocation = _mapCell.GridPostion;
        if(gridLocation == pawnGridPosition) { return; }
        mapCells[gridLocation.x, gridLocation.y].isObstacle = !mapCells[gridLocation.x, gridLocation.y].isObstacle;
        _mapCell.SetAsObstacle(mapCells[gridLocation.x, gridLocation.y].isObstacle);
     }

    private void ResolveClick_PlacingPawn(MapCell _mapCell)
    {
        Vector2Int clickPos = _mapCell.GridPostion;
        if(mapCells[clickPos.x, clickPos.y].isObstacle) { return; }
        pawnGridPosition = clickPos;

        GameEvents.PublishOnPawnLocationChangeRequest(_mapCell.transform.position);
    }

    private void ResolveClick_PawnMovement(MapCell _mapCell)
    {
        if(followPathInProgress) { return; }

        Vector2Int clickPos = _mapCell.GridPostion;
        if(mapCells[clickPos.x, clickPos.y].isObstacle) { return; }

        List<Vector2Int> path = pathFinder.CalculatePath(pawnGridPosition, clickPos);
        if(path.Count == 0) { return; }

        List<Vector3> pathAsWorldLocations = GetWorldLocations(path);
        pathVisualizer.DrawPath(pathAsWorldLocations);
        GameEvents.PublishOnFollowPathRequest(pathAsWorldLocations, path[0], FollowPathFinishedCallback, PathSegmentFinishedCallback);

        followPathInProgress = true;
    }

    private List<Vector3> GetWorldLocations(List<Vector2Int> path)
    {
        List<Vector3> worldLocations = new List<Vector3>();

        foreach(var pathElement in path)
        {
            worldLocations.Add(mapCells[pathElement.x, pathElement.y].mapCell.transform.position);
        }

        return worldLocations;
    }

    private void FollowPathFinishedCallback(Vector2Int gridPositionOnFinish)
    {
        pawnGridPosition = gridPositionOnFinish;
        followPathInProgress = false;
    }

    private void PathSegmentFinishedCallback()
    {
        pathVisualizer.RemoveSegment();
    }

    private Vector3 GetDesiredCellPostion(int _column, int _row)
    {
        return new Vector3(_column * (cellSize.x + gapBetweenCells), _row * (cellSize.y + gapBetweenCells) * -1, 0f);
    }

    private void OnDestroy()
    {
        UnregisterEvents();
    }

    private void UnregisterEvents()
    {
        for (int y = 0; y < mapSize.y; y++)
            for (int x = 0; x < mapSize.x; x++)
                mapCells[x, y].mapCell.OnClick -= MapCell_OnClick;
    }

    private void PrintLogs_DEBUG(int x, int y)
    {
        Debug.Log($"[{x},{y}]Summary cost: {mapCells[x,y].summaryCost}, " +
            $"Walking cost: {mapCells[x,y].walkingCost}, Heuristic cost: {mapCells[x,y].heuristicCost}");
    }
}
