// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com


public struct MapCellInfo
{

    public int walkingCost;
    public int heuristicCost;
    public int summaryCost;

    public bool isObstacle;
    public MapCell mapCell;

    public bool wasScanned;
    public bool costCalculated;

    public void ApplyCost()
    {
        summaryCost = walkingCost + heuristicCost;
    }

    public void Clear()
    {
        walkingCost = 0; 
        heuristicCost = 0; 
        summaryCost = 0;
        wasScanned = false;
        costCalculated = false;
    }

    public void ClearCalculatedState()
    {
        costCalculated = false;
    }

    public void ClearScannedState()
    {
        wasScanned = false;
    }

}
