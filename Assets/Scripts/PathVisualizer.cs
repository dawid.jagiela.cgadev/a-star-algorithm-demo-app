// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using System.Collections.Generic;
using UnityEngine;


public class PathVisualizer : MonoBehaviour
{

    [SerializeField] private LineRenderer lineRenderer;

    [SerializeField] private Color lineColor;


    private void Start()
    {
        lineRenderer.startColor = lineColor;
        lineRenderer.endColor = lineColor;
    }

    public void DrawPath(List<Vector3> _path)
    {
        if(_path.Count < 1) { return; }
        lineRenderer.positionCount = _path.Count;
        lineRenderer.SetPositions(_path.ToArray());
    }

    public void ClearPath()
    {
        lineRenderer.positionCount = 0;
    }

    public void RemoveSegment()
    {
        lineRenderer.positionCount--;
    }

}
