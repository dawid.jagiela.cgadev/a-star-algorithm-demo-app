// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

using System.Collections.Generic;
using System.Text;
using UnityEngine;


public class PathFinder
{

    private const int STRAIGHT_LINE_COST = 10;
    private const int DIAGONAL_LINE_COST = 14;

    private MapCellInfo[,] mapCells;

    private int mapWidth;
    private int mapHeight;

    private Queue<Vector2Int> cellsToScan = new();

    private Vector2Int startingPosition;
    private Vector2Int targetPosition;

    private Vector2Int[] offsets = {
         Vector2Int.up,
         Vector2Int.right,
         Vector2Int.down,
         Vector2Int.left,
         new Vector2Int(-1,1),
         new Vector2Int(1,1),
         new Vector2Int(1,-1),
         new Vector2Int(-1,-1)
    };         


    public PathFinder(MapCellInfo[,] _mapCells)
    {
        mapCells = _mapCells;
        mapWidth = _mapCells.GetLength(0);
        mapHeight = _mapCells.GetLength(1);
    }


    public List<Vector2Int> CalculatePath(Vector2Int _startingPosition, Vector2Int _targetPosition)
    {
        if(_startingPosition.Equals(_targetPosition))
        {
            return new();
        }

        startingPosition = _startingPosition;
        targetPosition = _targetPosition;

        ClearCellsInfo();

        bool targetReached = CalculateWalkingCost();
        if(!targetReached) { return new(); }

        ClearCalculatedState();
        CalculateHeuristicCost();
        ApplyCosts();

        ClearScannedState();
        List<Vector2Int> path = new();
        GeneratePath(path);

#if UNITY_EDITOR
        PrintPath_DEBUG(path);
#endif

        return path;
    }


    private void ClearCellsInfo()
    {
        for(int y = 0; y < mapHeight; y++)
            for (int x = 0; x < mapWidth; x++)
                mapCells[x, y].Clear();
    }


    private bool CalculateWalkingCost()
    {
        bool targetPositionReached = false;
        cellsToScan.Clear();
        
        cellsToScan.Enqueue(startingPosition);
        mapCells[startingPosition.x, startingPosition.y].costCalculated = true;
        mapCells[startingPosition.x, startingPosition.y].wasScanned = true;

        while(!targetPositionReached && cellsToScan.Count != 0)
        {
            CalculateNeighbouringCellsWalkingCost(cellsToScan.Dequeue(), ref targetPositionReached);
        }
        return targetPositionReached;
    }

    private void CalculateNeighbouringCellsWalkingCost(Vector2Int _centralPosition, ref bool _targetReached)
    {
        foreach(var offset in offsets)
        {
            Vector2Int neighbourPosition = new Vector2Int(_centralPosition.x + offset.x, _centralPosition.y + offset.y);

            if(!IsPositionValid(neighbourPosition.x, neighbourPosition.y)
                || mapCells[neighbourPosition.x, neighbourPosition.y].isObstacle
                || mapCells[neighbourPosition.x, neighbourPosition.y].costCalculated)
            {
                continue;
            }

            int currentWalkingCost = mapCells[_centralPosition.x, _centralPosition.y].walkingCost;
            if(IsDiagonal(offset.x, offset.y))
            {
                mapCells[neighbourPosition.x, neighbourPosition.y].walkingCost = currentWalkingCost + DIAGONAL_LINE_COST;
            }
            else
            {
                mapCells[neighbourPosition.x, neighbourPosition.y].walkingCost = currentWalkingCost + STRAIGHT_LINE_COST;
            }
            mapCells[neighbourPosition.x, neighbourPosition.y].costCalculated = true;

            if (!mapCells[neighbourPosition.x, neighbourPosition.y].wasScanned)
            {
                cellsToScan.Enqueue(neighbourPosition);
            }
        }
            
        if(_centralPosition.Equals(targetPosition))
        {
            mapCells[_centralPosition.x, _centralPosition.y].wasScanned = true;
            _targetReached = true;
            return;
        }

        mapCells[_centralPosition.x, _centralPosition.y].wasScanned = true;
    }


    private void ClearCalculatedState()
    {
        for(int y = 0; y < mapHeight; y++)
            for (int x = 0; x < mapWidth; x++)
                mapCells[x, y].ClearCalculatedState();
    }


    private void CalculateHeuristicCost()
    {
        bool startingPositionReached = false;
        cellsToScan.Clear();
        
        cellsToScan.Enqueue(targetPosition);
        mapCells[targetPosition.x, targetPosition.y].costCalculated = true;
        mapCells[targetPosition.x, targetPosition.y].wasScanned = true;

        while(!startingPositionReached && cellsToScan.Count != 0)
        {
            CalculateNeighbouringCellsHeuristicCost(cellsToScan.Dequeue(), ref startingPositionReached);
        }
    }

    private void CalculateNeighbouringCellsHeuristicCost(Vector2Int _centralPosition, ref bool _startingPositionReached)
    {
        foreach(var offset in offsets)
        {
            Vector2Int neighbourPosition = new Vector2Int(_centralPosition.x + offset.x, _centralPosition.y + offset.y);

            if (!IsPositionValid(neighbourPosition.x, neighbourPosition.y)
                || mapCells[neighbourPosition.x, neighbourPosition.y].isObstacle
                || mapCells[neighbourPosition.x, neighbourPosition.y].costCalculated)
            {
                continue;
            }

            int currentWalkingCost = mapCells[_centralPosition.x, _centralPosition.y].heuristicCost;
            if (IsDiagonal(offset.x, offset.y))
            {
                mapCells[neighbourPosition.x, neighbourPosition.y].heuristicCost = currentWalkingCost + DIAGONAL_LINE_COST;
            }
            else
            {
                mapCells[neighbourPosition.x, neighbourPosition.y].heuristicCost = currentWalkingCost + STRAIGHT_LINE_COST;
            }
            mapCells[neighbourPosition.x, neighbourPosition.y].costCalculated = true;

            if (!mapCells[neighbourPosition.x, neighbourPosition.y].wasScanned)
            {
                cellsToScan.Enqueue(neighbourPosition);
            }
        }

        if(_centralPosition.Equals(startingPosition))
            {
                mapCells[_centralPosition.x, _centralPosition.y].wasScanned = true;
                _startingPositionReached = true;
                return;
            }
            
            mapCells[_centralPosition.x, _centralPosition.y].wasScanned = true;
    }


    private void ApplyCosts()
    {
        for(int y = 0; y < mapCells.GetLength(1); y++)
            for(int x = 0; x < mapCells.GetLength(0); x++)
                mapCells[x,y].ApplyCost();
    }


    private void ClearScannedState()
    {
        for(int y = 0; y < mapHeight; y++)
            for (int x = 0; x < mapWidth; x++)
                mapCells[x, y].ClearScannedState();
    }


    private void GeneratePath(List<Vector2Int> _path)
    {
        Vector2Int cellToScan = targetPosition;
        mapCells[cellToScan.x, cellToScan.y].wasScanned = true;
        _path.Add(cellToScan);

        while(!cellToScan.Equals(startingPosition))
        {
            Vector2Int smallestNeighbourCostOffset = Vector2Int.zero;
            int smallestNeighbourCost = mapCells[cellToScan.x, cellToScan.y].summaryCost;

            for(int dy = -1; dy <= 1; dy++)
                for(int dx = -1; dx <= 1; dx++)
                {
                    Vector2Int neighbour = new Vector2Int(cellToScan.x + dx, cellToScan.y + dy);
                    if(dx == 0 && dy == 0
                        || !IsPositionValid(neighbour.x, neighbour.y)
                        || mapCells[neighbour.x, neighbour.y].isObstacle
                        || mapCells[neighbour.x, neighbour.y].wasScanned) { continue; }

                    if(mapCells[neighbour.x, neighbour.y].summaryCost <= smallestNeighbourCost)
                    {
                        smallestNeighbourCost = mapCells[neighbour.x, neighbour.y].summaryCost;
                        smallestNeighbourCostOffset.x = dx;
                        smallestNeighbourCostOffset.y = dy;
                    }
                }

            if(smallestNeighbourCostOffset != Vector2Int.zero)
            {
                cellToScan += smallestNeighbourCostOffset;
                mapCells[cellToScan.x, cellToScan.y].wasScanned = true;
                _path.Add(cellToScan);
            }
        }
        if(_path.Count > 0) { _path.RemoveAt(_path.Count-1); }
    }


    private bool IsPositionValid(int _x, int _y)
    {
        return !((_x < 0 || _x >= mapWidth || _y < 0 || _y >= mapHeight));
    }

    private bool IsDiagonal(int dx, int dy)
    {
        return (dx == -1 && dy == -1 || dx == 1 && dy == -1 || dx == -1 && dy == 1 || dx == 1 && dy == 1);
    }


    private void PrintPath_DEBUG(List<Vector2Int> _path)
    {
        StringBuilder sb = new StringBuilder();
        foreach(var pathElement in _path)
        {
            sb.AppendFormat("[{0},{1}] SC:{2}, WC:{3}, HC:{4} \n", pathElement.x, pathElement.y,
                mapCells[pathElement.x, pathElement.y].summaryCost,
                mapCells[pathElement.x, pathElement.y].walkingCost,
                mapCells[pathElement.x, pathElement.y].heuristicCost
                );
        }
        Debug.Log(sb.ToString());
    }
}
