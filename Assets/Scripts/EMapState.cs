// A* Algorithm Demo, (C) 2024 Dawid Jagie�a
// e-mail: dawid.jagiela@hotmail.com

public enum EMapState
{
    PlacingObstacles,
    PlacingPawn,
    PawnMovement
}
